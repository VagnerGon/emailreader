//
// Este arquivo foi gerado pela Arquitetura JavaTM para Implementa��o de Refer�ncia (JAXB) de Bind XML, v2.2.8-b130911.1802 
// Consulte <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas as modifica��es neste arquivo ser�o perdidas ap�s a recompila��o do esquema de origem. 
// Gerado em: 2014.05.27 �s 08:06:06 PM BRT 
//


package modeloboladao;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de ItemdeLinha complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conte�do esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ItemdeLinha">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="quantidade" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="produto" type="{}Produto"/>
 *         &lt;element name="idLinha" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ItemdeLinha", propOrder = {
    "quantidade",
    "produto",
    "idLinha"
})
public class ItemdeLinha {

    @XmlElement(required = true)
    protected BigInteger quantidade;
    @XmlElement(required = true)
    protected Produto produto;
    @XmlElement(required = true)
    protected BigInteger idLinha;

    /**
     * Obt�m o valor da propriedade quantidade.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getQuantidade() {
        return quantidade;
    }

    /**
     * Define o valor da propriedade quantidade.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setQuantidade(BigInteger value) {
        this.quantidade = value;
    }

    /**
     * Obt�m o valor da propriedade produto.
     * 
     * @return
     *     possible object is
     *     {@link Produto }
     *     
     */
    public Produto getProduto() {
        return produto;
    }

    /**
     * Define o valor da propriedade produto.
     * 
     * @param value
     *     allowed object is
     *     {@link Produto }
     *     
     */
    public void setProduto(Produto value) {
        this.produto = value;
    }

    /**
     * Obt�m o valor da propriedade idLinha.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getIdLinha() {
        return idLinha;
    }

    /**
     * Define o valor da propriedade idLinha.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setIdLinha(BigInteger value) {
        this.idLinha = value;
    }

    public ItemdeLinha() {
    }

    public ItemdeLinha(BigInteger quantidade, Produto produto, BigInteger idLinha) {
        this.quantidade = quantidade;
        this.produto = produto;
        this.idLinha = idLinha;
    }

    
}
