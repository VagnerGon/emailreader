//
// Este arquivo foi gerado pela Arquitetura JavaTM para Implementa��o de Refer�ncia (JAXB) de Bind XML, v2.2.8-b130911.1802 
// Consulte <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas as modifica��es neste arquivo ser�o perdidas ap�s a recompila��o do esquema de origem. 
// Gerado em: 2014.05.27 �s 08:06:06 PM BRT 
//


package modeloboladao;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de Software complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conte�do esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="Software">
 *   &lt;complexContent>
 *     &lt;extension base="{}Produto">
 *       &lt;sequence>
 *         &lt;element name="version" type="{http://www.w3.org/2001/XMLSchema}float"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Software", propOrder = {
    "version"
})
public class Software
    extends Produto
{

    protected float version;

    /**
     * Obt�m o valor da propriedade version.
     * 
     */
    public float getVersion() {
        return version;
    }

    /**
     * Define o valor da propriedade version.
     * 
     */
    public void setVersion(float value) {
        this.version = value;
    }
    
    public String getQuery(){
        return "insert into Produto values("+this.idProduto+",'"+this.descricao+"',"+this.preco+","+this.version+",null,"+this.fornecedor.number+");";
    }

    public Software() {
    }

    public Software(float version, BigInteger idProduto, String descricao, float preco, Fornecedor fornecedor) {
        super(idProduto, descricao, preco, fornecedor);
        this.version = version;
    }

    

    
}
