//
// Este arquivo foi gerado pela Arquitetura JavaTM para Implementa��o de Refer�ncia (JAXB) de Bind XML, v2.2.8-b130911.1802 
// Consulte <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas as modifica��es neste arquivo ser�o perdidas ap�s a recompila��o do esquema de origem. 
// Gerado em: 2014.05.27 �s 08:06:06 PM BRT 
//


package modeloboladao;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de Hardware complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conte�do esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="Hardware">
 *   &lt;complexContent>
 *     &lt;extension base="{}Produto">
 *       &lt;sequence>
 *         &lt;element name="assembly" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Hardware", propOrder = {
    "assembly"
})
public class Hardware
    extends Produto
{

    @XmlElement(required = true)
    protected String assembly;

    /**
     * Obt�m o valor da propriedade assembly.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAssembly() {
        return assembly;
    }

    /**
     * Define o valor da propriedade assembly.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAssembly(String value) {
        this.assembly = value;
    }
    
    public String getQuery(){
        return "Insert into Produto values("+this.idProduto+",'"+this.descricao+"',"+this.preco+",null,'"+this.assembly+"',"+this.fornecedor.getNumber()+");";

    }

    public Hardware() {
    }

    public Hardware(String assembly, BigInteger idProduto, String descricao, float preco, Fornecedor fornecedor) {
        super(idProduto, descricao, preco, fornecedor);
        this.assembly = assembly;
    }
    
    
}
