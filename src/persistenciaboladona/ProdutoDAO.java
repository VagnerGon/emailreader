/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package persistenciaboladona;
import java.sql.SQLException;
import java.util.List;
import modeloboladao.Fornecedor;
import modeloboladao.Produto;
/**
 *
 * @author Junin
 */
public interface ProdutoDAO extends DAOGeneric<Produto> {
    public List<Produto>obterTodos(List<Fornecedor> lista) throws SQLException;
    
}
