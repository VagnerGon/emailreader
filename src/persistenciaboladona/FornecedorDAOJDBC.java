/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package persistenciaboladona;

import java.math.BigInteger;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import modeloboladao.Endereco;
import modeloboladao.Fornecedor;

/**
 *
 * @author 20101bsi0011
 */
public class FornecedorDAOJDBC implements FornecedorDAO {
    public void insere (Fornecedor obj) throws SQLException{
        Connection con = Conexao.getConexao();
        Conexao.inicia();
        //String query="insert into Fornecedor values("+obj.getNumber()+",'"+obj.getEndereco().getState()+"','"+obj.getEndereco().getCity()+"','"+obj.getEndereco().getCountry()+"','"+obj.getEndereco().getStreet()+"','"+obj.getEndereco().getPostalCode()+"');";
        Conexao.comando(obj.getQuery());
        System.out.println(obj.getQuery());
        Conexao.fechar();
    }
    
    public List<Fornecedor> obterTodos() throws SQLException{
        List<Fornecedor> lista=new ArrayList();
        ResultSet rs= Conexao.Consulta("Select * from Fornecedor");
        
        while (rs.next()){
            Endereco e=new Endereco(rs.getString("street"),rs.getString("city"),rs.getString("state"),rs.getString("postalcode"),rs.getString("country"));
            BigInteger num=new BigInteger(rs.getString("number"));
            lista.add(new Fornecedor(num,e));
        }
        
        return lista;
    }
    
}
