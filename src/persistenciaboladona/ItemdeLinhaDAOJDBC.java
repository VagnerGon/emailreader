/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package persistenciaboladona;

import java.math.BigInteger;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import modeloboladao.ItemdeLinha;
import modeloboladao.Produto;

/**
 *
 * @author Junin
 */
public class ItemdeLinhaDAOJDBC implements ItemdeLinhaDAO{
    
    public void insere(ItemdeLinha obj) throws SQLException{
        Connection con = Conexao.getConexao();
        Conexao.inicia();
        String query="Insert into ItemdeLinha values("+obj.getIdLinha()+","+obj.getQuantidade()+",'"+obj.getProduto().getDescricao()+"',"+obj.getProduto().getIdProduto()+");";
        Conexao.comando(query);
        Conexao.fechar();
        
    }
    
    public List<ItemdeLinha> obterTodos() throws SQLException{
        return null;
    }
    
    public List<ItemdeLinha> obterTodos(List<Produto> prod) throws SQLException{
        List<ItemdeLinha> lista=new ArrayList();
        ResultSet rs=Conexao.Consulta("Select * From ItemdeLinha;");
        while(rs.next()){
            BigInteger idLinha=new BigInteger(rs.getString("idItem"));
            BigInteger idproduto=new BigInteger(rs.getString("idproduto"));
            BigInteger qtd=new BigInteger(rs.getString("qtd"));
            Produto p=null;
            for (int i=0;i<prod.size();i++){
                if (idproduto.compareTo(prod.get(i).getIdProduto())==0){
                    p=prod.get(i);
                    break;
                }
            }
            lista.add(new ItemdeLinha(qtd,p,idLinha));
        }
        
        return lista;
    }
}
