/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package persistenciaboladona;
import java.sql.SQLException;
import java.util.List;
import modeloboladao.Cliente;
import modeloboladao.Pedido;
/**
 *
 * @author Junin
 */
public interface PedidoDAO extends DAOGeneric<Pedido> {
    public List<Pedido> obterTodos(List<Cliente> clientes) throws SQLException;
    
}
