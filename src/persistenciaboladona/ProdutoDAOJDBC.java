/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package persistenciaboladona;


import java.math.BigInteger;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import modeloboladao.Fornecedor;
import modeloboladao.Hardware;
import modeloboladao.Produto;
import modeloboladao.Software;

public class ProdutoDAOJDBC implements ProdutoDAO{
    
    public void insere(Produto obj) throws SQLException{
        Connection con = Conexao.getConexao();
        Conexao.inicia();
        
        Conexao.comando(obj.getQuery());
        System.out.println(obj.getQuery());
        Conexao.fechar();
    }
    
    public List<Produto>obterTodos() throws SQLException{
        return null;
       
    }
        
    public List<Produto>obterTodos(List<Fornecedor> lista) throws SQLException{
        List<Produto> prod=new ArrayList();
        ResultSet rs=Conexao.Consulta("Select * from Produto");
        while (rs.next()){
            Produto p;
            float price=Float.parseFloat(rs.getString("unitePrice"));
            BigInteger idproduto=new BigInteger(rs.getString("idProduto"));
            BigInteger number=new BigInteger(rs.getString("idForneceodr"));
            Fornecedor f=null;
            for (int i=0;i<lista.size();i++){
                if (number.compareTo(lista.get(i).getNumber())==0){
                    f=lista.get(i);
                    break;
                }
            }
            
            if (rs.getString("assembly")==null){
                float version=Float.parseFloat(rs.getString("version"));
                p=new Software(version,idproduto,rs.getString("description"),price,f);
            }
            else{
                p=new Hardware(rs.getString("Assembly"),idproduto,rs.getString("description"),price,f);
            }
            prod.add(p);
        }
            
        return prod;
    }
        
       
    
    
    
    
}
