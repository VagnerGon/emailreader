/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package persistenciaboladona;

import java.math.BigInteger;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import modeloboladao.Cliente;
import modeloboladao.Endereco;

/**
 *
 * @author Junin
 */
public class ClienteDAOJDBC implements ClienteDAO {
    public void insere (Cliente obj) throws SQLException{
        Connection con = Conexao.getConexao();
        Conexao.inicia();
        String query="Insert into Cliente values("+obj.getCostumerID()+",'"+obj.getName()+"','"+obj.getEndereco().getState()+"','"+obj.getEndereco().getCity()+"','"+obj.getEndereco().getCountry()+"','"+obj.getEndereco().getStreet()+"','"+obj.getEndereco().getPostalCode()+"');";
        Conexao.comando(query);
        System.out.println(query);
        Conexao.fechar();
    }
    
    public List<Cliente> obterTodos() throws SQLException{
        List<Cliente> lista=new ArrayList();
        ResultSet rs=Conexao.Consulta("select * from Cliente;");
        while (rs.next()){
            Endereco e=new Endereco(rs.getString("street"),rs.getString("city"),rs.getString("state"),rs.getString("postalcode"),rs.getString("country"));
            BigInteger id=new BigInteger(rs.getString("costumerID"));
            lista.add(new Cliente(id,rs.getString("name"),e));
        }
        return lista;
    }
    
}
