/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package persistenciaboladona;

/**
 *
 * @author Junin
 */
public class FabricaDAO {
    private String tipoFabrica;
    private static FabricaDAO instance;
     
    public static FabricaDAO getInstance() {
        if (instance == null) instance = new FabricaDAO();
        return instance;
    }

    public String getTipoFabrica() {
        return tipoFabrica;
    }

    public void setTipoFabrica(String tipoFabrica) {
        this.tipoFabrica = tipoFabrica;
    }
    
    public DAOGeneric obterDAO(Class classe) {
        this.setTipoFabrica("JDBC");
        // monta o nome do DAO correspondente a classe passada
        String nome = classe.getName();
        //System.out.println(nome);
        nome = nome.replaceAll("Modelo", "Persistencia");
        //System.out.println(nome);
        nome = nome + "DAO" + this.getTipoFabrica();
        //System.out.println(nome);
        DAOGeneric dao = null;
        try {
            dao = (DAOGeneric) Class.forName(nome).newInstance();
        } catch (ClassNotFoundException | InstantiationException | 
                IllegalAccessException ex) {
            ex.printStackTrace();
        }
        return dao;
    }
}
