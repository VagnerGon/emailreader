/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package persistenciaboladona;
import java.sql.SQLException;
import java.util.List;
/**
 *
 * @author Junin
 */
public interface DAOGeneric<T> {
    public void insere(T obj) throws SQLException;
    public List<T> obterTodos() throws SQLException;
    //public void remover(int id) throws SQLException;
    //public void altera(T obj,int id) throws SQLException;
}
