/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistenciaboladona;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Conexao {

    public static Connection getConexao() {

        Connection conexao = null;

        try {
            Class.forName(Conf.driver);
            conexao = DriverManager.getConnection(Conf.url);
        } catch (ClassNotFoundException ex) {
            System.err.println("Driver de conexão não encontrado!");
        } catch (SQLException ex) {
            System.err.println("Não foi possível estabelecer conexão com o banco: " + Conf.url);
            ex.printStackTrace();
        } finally {
            return conexao;
        }
    }

    public static void inicia() throws SQLException {
        Statement st = Conexao.getConexao().createStatement();

        st.executeUpdate("create table if not exists Cliente(costumerID integer PRIMARY KEY, name VARCHAR(50), state VARCHAR (20), city  VARCHAR(30),  country VARCHAR(30),street VARCHAR(30), postalcode VARCHAR(30));");
        st.executeUpdate("create table if not exists Fornecedor(number integer Primary Key, state VARCHAR (20), city  VARCHAR(30), country VARCHAR(30),street VARCHAR(30), postalcode VARCHAR(30));");
        st.executeUpdate("create table if not exists Produto(idProduto integer PRIMARY KEY, description VARCHAR(50), unitePrice real, versao real, assembly VARCHAR(50),idFornecedor integer references Fornecedor(number)); ");
        st.executeUpdate("create table if not exists Pedido(number Integer Primary Key,idcliente integer references Cliente(costumerID) );");
        st.executeUpdate("create table if not exists ItemdeLinha(idItem integer Primary Key, qtd integer,idProduto integer references Produto(IdProduto));");

    }

    public static void comando(String query) throws SQLException {
        Statement st = Conexao.getConexao().createStatement();

        st.executeUpdate(query);
    }

    public static ResultSet Consulta(String query) throws SQLException {
        Statement st = Conexao.getConexao().createStatement();

        return st.executeQuery(query);
    }

    public static void fechar() throws SQLException {
        Conexao.getConexao().close();

    }
}
