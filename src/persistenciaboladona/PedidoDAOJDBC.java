/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package persistenciaboladona;

import java.math.BigInteger;
import modeloboladao.Pedido;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;
import modeloboladao.Cliente;

/**
 *
 * @author 20101bsi0011
 */
public class PedidoDAOJDBC implements PedidoDAO{
    public void insere(Pedido obj) throws SQLException{        
        Conexao.inicia();
        String query="Insert into Pedido values("+obj.getNumero()+","+obj.getCliente().getCostumerID()+");";
        Conexao.comando(query);
        Conexao.fechar();
    }
    
    public List<Pedido> obterTodos() throws SQLException{
        
        return null;
    }   
    
    public List<Pedido> obterTodos(List<Cliente> clientes) throws SQLException{
        List<Pedido> ped=new ArrayList();
        ResultSet rs=Conexao.Consulta("Select * from pedido;");
        
        while(rs.next()){
            BigInteger num=new BigInteger(rs.getString("number"));
            BigInteger id=new BigInteger(rs.getString("idCliente"));
            Cliente c=null;
            for (int i=0;i<clientes.size();i++){
                if (id.compareTo(clientes.get(i).getCostumerID())==0){
                    c=clientes.get(i);
                    break;
                }
            }
            ped.add(new Pedido(num,c));
        }
        
        return ped;
    }
}
