/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package persistenciaboladona;

import java.sql.SQLException;
import java.util.List;
import modeloboladao.ItemdeLinha;

/**
 *
 * @author Junin
 */
public interface ItemdeLinhaDAO extends DAOGeneric<ItemdeLinha> {
    
    public List<ItemdeLinha> obterTodos(List<modeloboladao.Produto> prod) throws SQLException;
    
}
