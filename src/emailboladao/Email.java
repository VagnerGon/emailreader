package emailboladao;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.Authenticator;
import javax.mail.Flags.Flag;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.search.FlagTerm;

/**
 *
 * @author vgon
 */
public class Email {

    private static final String PORTA = "465";
    private static final String USUARIO = "ce2014ifes@gmail.com";
    private static final String SENHA = "oadalobsuetam";    
    private static final String ANEXO = "resposta.xml";
    
    private static String destinatario;
    
    public boolean read() {
        Properties props = new Properties();

        props.setProperty("mail.store.protocol", "imaps");

        Session session = Session.getInstance(props, null);

        Store store;
        try {
            store = session.getStore();
            store.connect("imap.gmail.com", USUARIO, SENHA);

            Folder inbox = store.getFolder("INBOX");
            inbox.open(Folder.READ_WRITE);

            int naoLidas = inbox.getUnreadMessageCount();                         

            if (naoLidas != 0) {
                //Message msg = inbox.getMessage(naoLidas);
                Message messages[] = inbox.search(new FlagTerm(new Flags(Flag.SEEN), false));
                FetchProfile fp = new FetchProfile();
                fp.add(FetchProfile.Item.ENVELOPE);
                fp.add(FetchProfile.Item.CONTENT_INFO);
                inbox.fetch(messages, fp);
                Message msg = messages[0];
                Email.destinatario = msg.getFrom()[0].toString();
                Multipart mp = (Multipart) msg.getContent();                
                for (int i = 0; i < mp.getCount(); i++) {
                    BodyPart bp = mp.getBodyPart(i);                    
                    if (Part.ATTACHMENT.equalsIgnoreCase(bp.getDisposition())) {                        
                        savaArquivo(bp);
                    }
                }
                msg.setFlag(Flags.Flag.SEEN, true);//Marca como lida
            } else {
                System.out.println("Não há mensagens novas.");
                return false;
            }                   
            
            return true;
            
        } catch (NoSuchProviderException ex) {
            ex.printStackTrace();            
        } catch (MessagingException | IOException ex) {
            ex.printStackTrace();            
        }
        return false;
    }

    public void send(String mensagem, boolean anexo) {
        Properties props = new Properties();

        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.transport.protocol", "smtp"); //define protocolo de envio como SMTP
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.auth", "true"); //ativa autenticacao
        props.put("mail.smtp.user", USUARIO);
        props.put("mail.smtp.port", PORTA); //porta
        props.put("mail.smtp.socketFactory.port", PORTA);
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.socketFactory.fallback", "false");
        Session session = Session.getDefaultInstance(props, new Authenticator() {

            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(USUARIO, SENHA);
            }
        });
        //session.setDebug(true);

        //Objeto que contém a mensagem
        Message msg = new MimeMessage(session);

        try {
            //Setando o destinatário
            msg.setRecipient(Message.RecipientType.TO, new InternetAddress(this.destinatario));
            //Setando a origem do email
            msg.setFrom(new InternetAddress(USUARIO));
            //Setando o assunto
            msg.setSubject("Resposta - Pedido");
            //Setando o conteúdo/corpo do email
            msg.setContent(mensagem, "text/plain");
            //Seta a data
            msg.setSentDate(new Date());

            if(anexo){
                //Anexo
                MimeBodyPart messageBodyPart = new MimeBodyPart();
                Multipart multipart = new MimeMultipart();
                DataSource source = new FileDataSource(ANEXO);

                messageBodyPart.setDataHandler(new DataHandler(source));
                messageBodyPart.setFileName(ANEXO);
                multipart.addBodyPart(messageBodyPart);

                msg.setContent(multipart);
            }
            // evniando mensagem (tentando)
            Transport.send(msg);

        } catch (AddressException e) {
            System.err.println("Erro ao localizar endereço ou ao logar");
            e.printStackTrace();
        } catch (MessagingException e) {
            System.err.println("Erro ao enviar mensagem");
            e.printStackTrace();
        }
    }
    
    public void send() {
        send("Arquivo de resposta em anexo", true);
    }
    
    public void send(String email){
        this.destinatario = email;
    }
    
    public void sendErro(){
        send("Houve um erro durante a validação de seu pedido.", false);
    }

    private void savaArquivo(BodyPart bp) throws IOException, MessagingException {
        List<File> attachments = new ArrayList<>();
        InputStream is = bp.getInputStream();        
        String nome = bp.getFileName() == null ? "pedido.xml" : bp.getFileName();
        File f = new File("Anexos/" + nome);
        try (FileOutputStream fos = new FileOutputStream(f)) {
            byte[] buf = new byte[4096];
            int bytesRead;
            while ((bytesRead = is.read(buf)) != -1) {
                fos.write(buf, 0, bytesRead);
            }
        }
        attachments.add(f);
        System.out.println("Anexo salvo.");
    }
}
