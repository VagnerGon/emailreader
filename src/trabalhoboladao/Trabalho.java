/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalhoboladao;

import emailboladao.Email;
import java.sql.SQLException;
import javax.xml.parsers.ParserConfigurationException;
import modeloboladao.Pedido;
import org.xml.sax.SAXException;
import persistenciaboladona.Conexao;
import persistenciaboladona.PedidoDAOJDBC;
import xmlboladao.Xml;

/**
 *
 * @author 20101bsi0194
 */
public class Trabalho {

    
    private boolean emailMonitoring(){
        Email email = new Email();
        while(true){            
            try {
                Thread.sleep(10);
                 if(email.read())
                     return true;
            } catch(InterruptedException ex) {
                Thread.currentThread().interrupt();
            }           
        }
    }
    
    private void salvaPedido(Pedido p){
        try {
            Conexao.inicia();
            
            PedidoDAOJDBC daoPedido = new PedidoDAOJDBC();
            daoPedido.insere(p);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
    
    public static void main(String[] args) {
        
        Trabalho principal = new Trabalho(); 
        

        if (principal.emailMonitoring()){            
            try{
                Xml.XmlParser();
                System.out.println("Parse realisado com sucesso!");
            }catch(ParserConfigurationException | SAXException ex){
                System.err.println("Houve um erro no parse do arquivo");
                new Email().sendErro();
                return;
            }
            
            Pedido pedido;
            
            pedido = Xml.getPedidofromXML();
            
            principal.salvaPedido(pedido);
            
            principal.criaResposta(pedido);
            
            principal.enviarResposta();
            
        }                           
    }

    private void criaResposta(Pedido pedido) {
               
        Xml.createXMLresponse(pedido);                
                       
    }

    private void enviarResposta() {
        new Email().send();
    }
}
