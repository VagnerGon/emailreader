/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package xmlboladao;

import java.io.File;
import java.io.IOException;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import modeloboladao.Pedido;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

/**
 *
 * @author 20102bsi0278
 */
public class Xml {
   
    public static void XmlParser() throws SAXException, ParserConfigurationException{
        try {
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser saxParser = factory.newSAXParser();
            XMLReader reader = saxParser.getXMLReader();
            reader.parse(new InputSource("Anexos/teste.xml"));
        } catch (IOException ex) {
            System.err.println("Erro na leitura do arquivo");
        }
    }
    
    public static Pedido getPedidofromXML(){
        try{
            JAXBContext context = JAXBContext.newInstance(Pedido.class);
            Unmarshaller unmarshaller = context.createUnmarshaller();           
            return (Pedido)unmarshaller.unmarshal(new File("Anexos/teste.xml"));
        }catch(JAXBException ex){
            ex.printStackTrace();
            return null;            
        }
    }
    
    public static void createXMLresponse(Pedido p){
        try{
            JAXBContext context = JAXBContext.newInstance(Pedido.class);
            Marshaller m = context.createMarshaller();
            //for pretty-print XML in JAXB
            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            m.marshal(p, new File("resposta.xml"));
        }catch(JAXBException ex){
            ex.printStackTrace();
        }
    }
}
